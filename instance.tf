resource "google_compute_instance" "bt-kc01" {
	count = 1
	name = "bt-kc01-000${count.index}"
	machine_type = "f1-micro"
	zone = "${var.region_zone}"

	network_interface {
		#network = "${google_compute_network.platform.name}"
		subnetwork = "${google_compute_subnetwork.dev.name}"
		access_config {
			# 
		}
	}

	boot_disk {
		initialize_params {
			image = "centos-cloud/centos-7"
		}
	}
	
	metadata {
		sshKeys = "${var.gce_ssh_user}:${file(var.gce_ssh_pub_key_file)}"
	}

	metadata_startup_script = <<SCRIPT
${file("${path.module}/startup.sh")}
SCRIPT

}
