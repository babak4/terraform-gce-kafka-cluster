variable "gce_ssh_user" {
	default = "babak4"
}

variable "gce_ssh_pub_key_file" {
	default = "~/.ssh/google_compute_engine.pub"
}

variable "region" {
  default = "europe-west1"
}

variable "region_zone" {
  default = "europe-west1-b"
}

variable "project_name" {
  default = "light-client-198023"
}

variable "account_file_path" {
  default = "gc-cred.json"
}

variable "network_name" {
  default = "btnet01"
}
